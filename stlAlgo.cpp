/*

   Find keywords TODO's to fill in the missing codes

*/

#include <algorithm>
#include <random>
#include <set>
#include <map>
#include <iterator>
#include <iostream>

using namespace std;

/*
  TODO: 
  In this lab, we are going to use STL algorithms with lambda expressions to 
    -- Compute the mode, median, average, min, max, and geometric mean for a list of random integer
    -- Overload operator +.-,&& to implement the union, difference, and intersection of two sets.

*/


template <class T1, class T2>
ostream&   operator<< (ostream& out, const pair<T1,T2>& x ) {
  cout << "(" << x.first << ", "  << x.second << ") " ;
  return out;
}

/*
  TODO: Overload operator<< for map<T1,T2>, using the STL algorithm 
  for_each or copy.
  (Answers are given as the comments.)
*/
template<
  template <typename... Args> class Container,
  typename T
>
ostream& operator<< (ostream& out, const Container<T>& s ) {

  // for_each( cbegin(s), cend(s), [&out](auto c) {out << c << " ";} );
  // copy(cbegin(s), cend(s), ostream_iterator< decltype(*s.begin()) >(out, ", "));
  return out;
}


/*
  TODO: overload operator && for set<T>
  Description: Given set A and set B, A && B should return the 
               intersection of A and B
  Implementation: 
      1. using STL algoirhtm set_intersection and iterator adaptor
      2. or, your own ways
      
  (Answers are given as the comments.)
*/
template <class T >
set<T> operator&& ( const set<T> & s1, const set<T> &s2) {
  set<T> s ;
  // insert_iterator<set<T>> inserter(s, begin(s));
  // set_intersection( cbegin(s1), cend(s1), cbegin(s2), cend(s2), inserter);

  return move(s); // move semantics
}

/*
  TODO: overload operator +  for set<T>
  Description: Given set A and set B, A + B should return the union of A and B
  Implementation: 
      1. using STL algoirhtm set_union and iterator adaptor
      2. or, your own ways
*/
template <class T >
set<T> operator+ ( const set<T> & s1, const set<T> &s2) {
  set<T> s ;

  // fill in your codes here
  return s;
}

/*
  TODO: overload operator -  for set<T>
  Description: Given set A and set B, A - B should return the 
               difference of A and B
  Implementation: 
      1. using STL algoirhtm set_difference and iterator adaptor
      2. or, your own ways
*/
template <class T >
set<T> operator- ( const set<T> & s1, const set<T> &s2) {
  set<T> s ;
  // fill in your codes here
  return s; // move semantics
}


template<class T>
void unitTestSet () {
  cout << endl << "Entering function --> " << __func__ << endl;
  set<T> s1 = { 1,2,3,4};
  set<T> s2 = { 1,3,12,13};

  set<T> s = s1 && s2; 
  cout << "s1 = " << s1  << endl;
  cout << "s2 = " << s2  << endl;
  cout << "s1 && s2 = " << s  << endl;
  cout << "s1 + s2 = " << s1 + s2 << endl;
  cout << "s1 - s2 = " << s1 - s2 << endl;
  
}


// a demo of how to use lambda
void unitTestLambda () {
  cout << endl << "Entering function --> " << __func__ << endl;

  double data = 1.23;
  auto foo= [data]{ cout << "Data = " << data << endl; } ;
  auto bar= [data] () mutable { data *= 2;}; // non-const
  foo ();
  bar ();
  cout << "Data = " << data << endl;

}


/*
  TODO: get the geometric mean of a given vector<T>
  Implementation: 
      using STL Algorithms accumulate with transparent functor multiplies
      or using STL algorithm for_each 
*/
template <class T>
double getGeometricMean ( const vector<T>& v ) {
  // fill in your codes here
  return 0;
}

/*
  TODO: get the average of a given vector<T>
  Implementation: using STL algorithm accumulate
*/
template <class T>
double getAverage ( const vector<T>& v ) {
  // fill in your codes here
  return  0;
}

/*
  TODO: get the median of a given vector<T>
  Implementation: using STL algorithm sort 
*/
template <class T>
double getMedian ( const vector<T>& v ) {
  // fill in your codes
  return 0.0 ;
}

/*
  TODO: get the mode of a given vector<T>
  Implementation: using STL container map<T, size_t>
  The mode can be multiple numbers, so getMode should return a set of numbers.
*/
template <class T>
set<T> getMode ( const vector<T>& v ) {
  set<T> s ;

  // fill in your codes
  return move(s) ; //
}

template <class T>
void unitTestVector  () {
  cout << endl << "Entering function --> " << __func__ << endl;
  vector<T> v = { 1,1,2,2,3,3,3,4,5,7,7,7,7,8,9,9,1,1,6} ;
  cout << "Given random numbers --> " << v << endl;
  sort( begin(v), end(v) );
  cout << "The sorted list      --> " << v << endl;
  set<T> s = getMode ( v );
  double median = getMedian ( v );
  double average = getAverage ( v );
  double geomean = getGeometricMean ( v );
  cout << "The mode is " << s <<  endl;
  cout << "The median is " << median <<  endl;
  cout << "The average is " << average <<  endl;
  cout << "The geometric mean  is " << geomean <<  endl;
}

int main () {
  unitTestVector<int> ();
  unitTestSet<int> () ;
}

